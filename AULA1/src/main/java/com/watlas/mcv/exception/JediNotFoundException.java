package com.watlas.mcv.exception;

public class JediNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1l;

    public  JediNotFoundException(String message) {
        super(message);
    }

    public JediNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public JediNotFoundException() {

    }
}
