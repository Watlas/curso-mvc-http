package com.watlas.mcv.rest;

import com.watlas.mcv.exception.JediNotFoundException;
import com.watlas.mcv.model.Jedi;
import com.watlas.mcv.service.JediService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/jedi")
public class JediResource {

    @Autowired
    private JediService service;

    @GetMapping()
    public List<Jedi> getAll(){
        return service.findAll();
    }

    @GetMapping("{id}")
    public ResponseEntity<Jedi> getJedi(@PathVariable("id") Long id){ //path recupera  o ID da URL
        final Jedi jedi = service.findById(id);
        return ResponseEntity.ok(jedi);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Jedi> createJedi( @RequestBody Jedi obj){ //vai pegar o json(requestbody) emm json
        obj = service.save(obj);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Jedi> updateJedi(@PathVariable Long id, @RequestBody Jedi obj){
        final Jedi jedi = service.update(id, obj);

        return ResponseEntity.ok(jedi);

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id){
        service.delete(id);
        return ResponseEntity.noContent().build();
    }

}
