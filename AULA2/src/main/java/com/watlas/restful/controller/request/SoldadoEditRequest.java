package com.watlas.restful.controller.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SoldadoEditRequest {
    private String cpf;
    private String nome;
    private String raca;
    private String arma;
    private String status;
    
}
