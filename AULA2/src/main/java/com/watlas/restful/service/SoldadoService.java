package com.watlas.restful.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.watlas.restful.controller.request.SoldadoEditRequest;
import com.watlas.restful.controller.response.SoldadoListResponse;
import com.watlas.restful.controller.response.SoldadoResponse;
import com.watlas.restful.dto.Soldado;
import com.watlas.restful.entity.SoldadoEntity;
import com.watlas.restful.repository.SoldadoRepository;
import com.watlas.restful.resource.ResourceSoldado;
import org.springframework.hateoas.EntityModel;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SoldadoService {

    private SoldadoRepository soldadoRepository;
    private ObjectMapper objectMapper;
    private ResourceSoldado resourceSoldado;

    public SoldadoService(SoldadoRepository soldadoRepository, ObjectMapper objectMapper, ResourceSoldado resourceSoldado) {
        this.soldadoRepository = soldadoRepository;
        this.objectMapper = objectMapper;
        this.resourceSoldado = resourceSoldado;
    }

    public SoldadoResponse buscarSoldado(Long id) {
        SoldadoEntity soldado = soldadoRepository.findById(id).orElseThrow();
        SoldadoResponse soldadoResponse = objectMapper.convertValue(soldado, SoldadoResponse.class);
        return soldadoResponse;
    }

    public void criarSoldado(Soldado soldado){
        SoldadoEntity soldadoEntity = objectMapper.convertValue(soldado, SoldadoEntity.class);
        soldadoRepository.save(soldadoEntity);
    }

    public void alterarSoldado(Long id, SoldadoEditRequest soldadoEditRequest) {
        SoldadoEntity soldadoEntity = objectMapper.convertValue(soldadoEditRequest, SoldadoEntity.class);
        soldadoEntity.setId(id);
        soldadoRepository.save(soldadoEntity);
    }

    public void deletarSoldado(Long id) {
        SoldadoEntity soldado = soldadoRepository.findById(id).orElseThrow();
        soldadoRepository.delete(soldado);
    }

    public List<SoldadoListResponse> buscarSoldados(){
        List<SoldadoEntity> all = soldadoRepository.findAll();


        List<SoldadoListResponse> soldadoStream = all.stream()
                .map(it -> resourceSoldado.criarLink(it))
                .collect(Collectors.toList());
        return soldadoStream;
    }

//
//    public CollectionModel<SoldadoListResponse> buscarSoldados(){
//        List<SoldadoEntity> all = soldadoRepository.findAll();
//        Iterable<SoldadoListResponse> people;
//
//        List<SoldadoListResponse> soldadoStream = all.stream()
//                .map(it -> resourceSoldado.criarLink(it))
//                .collect(Collectors.toList());
//        return new CollectionModel<>(soldadoStream);
//    }







}
