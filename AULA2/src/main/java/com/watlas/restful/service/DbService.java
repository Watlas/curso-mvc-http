package com.watlas.restful.service;

import com.watlas.restful.entity.SoldadoEntity;
import com.watlas.restful.repository.SoldadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class DbService {

    @Autowired
    private SoldadoRepository repository;


    public void instanciaBanco(){

        SoldadoEntity s1 = new SoldadoEntity(null, "12345678910", "Watlas", "humana", "ak47", "o brabo");
        SoldadoEntity s2 = new SoldadoEntity(null, "09876543211", "Marcos", "humana", "faca", "o quase brabo");
        SoldadoEntity s3 = new SoldadoEntity(null, "15684675745", "Maria", "Cachorra", "sem arma", "status vazio");

        repository.saveAll(Arrays.asList(s1,s2,s3));

    }
}
