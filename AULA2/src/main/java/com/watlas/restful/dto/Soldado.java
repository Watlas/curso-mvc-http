package com.watlas.restful.dto;

import com.watlas.restful.enun.Raca;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Soldado {
    private Long id;
    private String cpf;
    private String nome;
    private Raca raca;
    private String arma;
    private String status;

    }

