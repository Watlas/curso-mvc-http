package com.watlas.restful.config.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.PackageVersion;
import com.watlas.restful.enun.Raca;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class Jackson {


    @Bean //pega o JSON E transfeorma no nosso objeto java
    public ObjectMapper ObjectMapper(){
        ObjectMapper objectMapper = new ObjectMapper();
        //Propiedades nao mapeadas na quebram
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        //falha se alguma estiver fazia
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        //serve para compatibilidade com arrays
        objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        //serializa datas
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.registerModule(racaModule());
        return objectMapper;
    }

    public SimpleModule racaModule(){
        SimpleModule dateModule = new SimpleModule("JSONRacaModule", PackageVersion.VERSION);
        dateModule.addSerializer(Raca.class, new RacaCerialize());
        dateModule.addDeserializer(Raca.class, new RacaDescerialize());
        return dateModule;
    }

    class RacaCerialize extends StdSerializer<Raca>{
        public RacaCerialize(){
            super(Raca.class);
        }

        @Override
        public void serialize(Raca value, JsonGenerator gen, SerializerProvider provider) throws IOException {
            gen.writeString(value.getValue());
        }

    }

    class RacaDescerialize extends StdDeserializer<Raca>{
        public RacaDescerialize(){
            super(Raca.class);
        }

        @Override
        public Raca deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
            return Raca.of(p.getText());
        }

    }

}
