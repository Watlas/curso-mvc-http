package com.watlas.restful.repository;

import com.watlas.restful.entity.SoldadoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SoldadoRepository extends JpaRepository<SoldadoEntity, Long> {
}
